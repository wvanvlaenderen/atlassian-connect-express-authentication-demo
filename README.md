# Atlassian Add-on using Express

Congratulations! You've successfully created an Atlassian Connect Add-on using the Express web application framework.

## What's next?

[Read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).


## How to get started
Copy the credentials.json.sample file to credentials.json and set credentials.

Run `npm install` to fetch node dependencies
Run `npm run start` to start the add-on, the add-on should register itself in Jira cloud. Check the logs for additional information regarding tunnels being created.